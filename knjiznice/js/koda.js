
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = "";
  var sessionId = getSessionId();

  // TODO: Potrebno implementirati
   $.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    async: false,
		    headers: {"Ehr-Session": sessionId},
		    success: function (data) {
		        
		        
		        ehrId = data.ehrId;
		        var partyData = {
		            1:{
		                 firstNames: "Janko",
		            lastNames: "Kamencek",
		            dateOfBirth: "1960-06-10T09:30",
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		            },
		            2:{
		                 firstNames: "Metka",
		            lastNames: "Kamencek",
		            dateOfBirth: "1960-06-10T09:30",
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		            }, 
		            3:{
		                 firstNames: "janez",
		            lastNames: "Kranjski",
		            dateOfBirth: "2050-06-10T09:30",
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		            }
		           
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            async: false,
		            headers: {"Ehr-Session": sessionId},
		            contentType: 'application/json',
		            data: JSON.stringify(partyData[stPacienta]),
		            success: function (party) {
		                
		                
		                var podatki = 
		                {1:{
		                	// Struktura predloge je na voljo na naslednjem spletnem naslovu:
                          // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
                    		    1:{"ctx/language": "en",
                    		    "ctx/territory": "SI",
                    		    "ctx/time": "2325",
                    		    "vital_signs/height_length/any_event/body_height_length": "160",
                    		    "vital_signs/body_weight/any_event/body_weight": "50"},
                    		    2:{
                    		    "ctx/language": "en",
                    		    "ctx/territory": "SI",
                    		    "ctx/time": "2312",
                    		     "vital_signs/height_length/any_event/body_height_length": "170",
                    		    "vital_signs/body_weight/any_event/body_weight": "77"},
                    		    3:{
                    		    "ctx/language": "en",
                    		    "ctx/territory": "SI",
                    		    "ctx/time": "2313",
                    		     "vital_signs/height_length/any_event/body_height_length": "180",
                    		    "vital_signs/body_weight/any_event/body_weight": "100"}
		                },
		                2:{
		                        1:{"ctx/language": "en",
                    		    "ctx/territory": "SI",
                    		    "ctx/time": "2325",
                    		    "vital_signs/height_length/any_event/body_height_length": "150",
                    		    "vital_signs/body_weight/any_event/body_weight": "50"},
                    		    2:{
                    		    "ctx/language": "en",
                    		    "ctx/territory": "SI",
                    		    "ctx/time": "2312",
                    		     "vital_signs/height_length/any_event/body_height_length": "155",
                    		    "vital_signs/body_weight/any_event/body_weight": "85"},
                    		    3:{
                    		    "ctx/language": "en",
                    		    "ctx/territory": "SI",
                    		    "ctx/time": "2313",
                    		     "vital_signs/height_length/any_event/body_height_length": "180",
                    		    "vital_signs/body_weight/any_event/body_weight": "76"}
		                },
		                3:{
		                        1:{"ctx/language": "en",
                    		    "ctx/territory": "SI",
                    		    "ctx/time": "2325",
                    		    "vital_signs/height_length/any_event/body_height_length": "125",
                    		    "vital_signs/body_weight/any_event/body_weight": "70"},
                    		    2:{
                    		    "ctx/language": "en",
                    		    "ctx/territory": "SI",
                    		    "ctx/time": "2312",
                    		     "vital_signs/height_length/any_event/body_height_length": "185",
                    		    "vital_signs/body_weight/any_event/body_weight": "80"},
                    		    3:{
                    		    "ctx/language": "en",
                    		    "ctx/territory": "SI",
                    		    "ctx/time": "2313",
                    		     "vital_signs/height_length/any_event/body_height_length": "190",
                    		    "vital_signs/body_weight/any_event/body_weight": "100"}
		                }
                    		};
                    		
                    		
                    		        var parametriZahteve = {
                        		    ehrId: ehrId,
                        		    templateId: 'Vital Signs',
                        		    format: 'FLAT',
                        		};
                        		for(var j = 0; j<3; j++){
                        		    $.ajax({
                        		    headers: {"Ehr-Session": sessionId},
                        		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
                        		    type: 'POST',
                        		    async: false,
                        		    contentType: 'application/json',
                        		    data: JSON.stringify(podatki[stPacienta][j+1]),
                        		    success: function (res) {
                        		        
                        		        console.log(ehrId);
                        		    },
                        		    
                        		    error: function(napaka){
                        		        console.log(napaka);
                        		    }
                        		    
                        		});
                        		}
                        		
                        		
                    		    
		                
		                
		            },
		            error: function(napaka3){
		                console.log(napaka3);
		            }
		        })
		        
		    } 
		        
  
  });
	$('#vnos1').val(ehrId);
  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

var tezacezcas=[];

function prikazipodatke(){
	var id= $('#vnos1').val();
	var sessionId = getSessionId();
	var teza= 0;
	var visina = 0;
	$('#teza_old').html("");
	$('#visina_old').html("");
	$('#graf').html("");
	$.ajax({
		    url: baseUrl + "/view/" + id + "/height",
		    type: 'GET',
		    headers: {"Ehr-Session": sessionId},
		    success: function (res) {
		    	for(var i = 1; i<res.length; i++){
		    		$('#visina_old').append(res[i].height + "  ");
		    	}
		    	$('#visina').html(res[0].height);
		    	visina = res[0].height;
		    	
		    	
		    	$.ajax({
		    url: baseUrl + "/view/" + id + "/weight",
		    type: 'GET',
		    headers: {"Ehr-Session": sessionId},
		    success: function (res) {
		    	for(var i = 1; i<res.length; i++){
		    		$('#teza_old').append(res[i].weight + "  ");
		    		
		    	}
		    	$('#teza').html(res[0].weight);
		        teza= res[0].weight;
		        for(var i = 0; i<res.length; i++){
		    		tezacezcas[i] = res[i].weight;
		    	}
		        
				ITM( visina, teza);
				
				
				
		    },
		    error: function() {
		    	console.log("error");
		    }
		});
		        
		 
		    },
		    error: function() {
		    	console.log("error");
		    }
		});
		
			
		
		
	
}

function ITM( visina, teza){
	if(visina==0 || teza == 0)
		return;
		
	var itm =teza/(visina/100*visina/100);
	$('#itm').html(itm);
	
	if(itm > 25){
			$('#nasvet').text("Vas telesna masa je prevelika!!!");
	}
	else if(itm < 17){
			$('#nasvet').text("ste podhranjeni!");
	}
	else{
			$('#nasvet').text("imate normalno telesno maso!");
	}
	
	
	
	var data=[];
	for(var i=0; i<tezacezcas.length; i++){
		data[i]={merjenje: i+1,
		teza:tezacezcas[i]};
		
	}
	
	new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'graf',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: data,
  // The name of the data record attribute that contains x-values.
  xkey: 'merjenje',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['teza'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Teza']
});
}


$(document).ready(function(){

 $('#visina').click(function(){
        if ( $('#visina_old').css('visibility') == 'hidden' )
		 $('#visina_old').css('visibility','visible');
		else
    		$('#visina_old').css('visibility','hidden');
    });	
    
  $('#teza').click(function(){
        if ( $('#teza_old').css('visibility') == 'hidden' )
		 $('#teza_old').css('visibility','visible');
		else
    		$('#teza_old').css('visibility','hidden');
    });	

});